ALTER TABLE record MODIFY type VARCHAR(10);

UPDATE record SET type='CD' WHERE type='0';
UPDATE record SET type='DVD' WHERE type='1';
UPDATE record SET type='Blu-Ray' WHERE type='2';
UPDATE record SET type='Manga' WHERE type='3';
UPDATE record SET type='Kirja' WHERE type='4';
UPDATE record SET type='Pokkari' WHERE type='5';
UPDATE record SET type='Sarjakuva' WHERE type='6';
UPDATE record SET type='Oppikirja' WHERE type='7';
UPDATE record SET type='Peli' WHERE type='8';