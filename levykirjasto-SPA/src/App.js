import React from 'react';
import { connect } from 'react-redux';
import {Container} from 'semantic-ui-react';

import './App.css';

import Header from './components/Header';
import RecordTable from './components/RecordTable';
import SearchBar from './components/SearchBar';
import records from './api/records';
import {searchAction, tableConfigAction} from './actions';
import TableConfig from "./classes/tableConfig";

class App extends React.Component {
    state = { sortColumn: null, sortDesc: false };

    componentDidMount() {
        records.get('').then(result => {
            const tableConfig: TableConfig = new TableConfig(result.data, result.data);
            this.props.tableConfigAction(tableConfig);
        });
    }

    render() {
        return (
            <div className="app">
                <Header title="Levykirjasto"/>
                <Container>
                    <SearchBar onChange={this.onSearchChanged}/>
                    <RecordTable
                        handlePaginationChange={this.handlePaginationChange}
                        sortDesc={this.state.sortDesc}
                        sortColumn={this.state.sortColumn}
                        onSort={this.onSort} />
                </Container>
            </div>
        );
    }

    handlePaginationChange = (activePage) => {
        this.props.tableConfigAction({activePage});
    };

    onSearchChanged = (searchPhrase) => {
        const filtered = this.filter(this.props.allRecords, searchPhrase);
        this.props.searchAction(searchPhrase);
        this.props.tableConfigAction({records: filtered, activePage: 1});
    };

    filter = (records, term) => {
        this.setState({sortDesc: false, sortColumn: null});
        this.props.tableConfigAction({activePage: 1});

        return records.filter((record) => {
            return record.ean.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
                record.name.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
                record.type.toLowerCase().indexOf(term.toLowerCase()) > -1;
        });
    };

    onSort = (columnName) => {
        let sortDesc = false;

        if (this.state.sortColumn === columnName) {
            sortDesc = !this.state.sortDesc;
        }

        const sortedRecords = sortDesc ? this.sortDesc(columnName) : this.sortAsc(columnName);
        const slicedRecords = this.slice(sortedRecords, 1);

        this.props.tableConfigAction({records: slicedRecords, activePage: 1});
        this.setState({sortDesc: sortDesc, sortColumn: columnName});
    };

    slice = (records, activePage) => {
        const start = activePage*50 - 50;
        const end = start + 50;
        return records.slice(start, end);
    };

    sortDesc = (columnName) => {
        return this.props.records.sort((a, b) => {
            if (a[columnName] < b[columnName]){
                return -1;
            }
            if (a[columnName] > b[columnName]){
                return 1;
            }
            return 0;
        });
    };

    sortAsc = (columnName) => {
        return this.props.records.sort((a, b) => {
            if (a[columnName] > b[columnName]){
                return -1;
            }
            if (a[columnName] < b[columnName]){
                return 1;
            }
            return 0;
        });
    };
}

const mapStateToProps = (state) => {
    return {
        records: state.tableConfig.records,
        allRecords: state.tableConfig.allRecords,
        searchPhrase: state.searchPhrase,
        activePage: state.tableConfig.activePage
    };
};

export default connect(mapStateToProps, {
    tableConfigAction,
    searchAction
})(App);
