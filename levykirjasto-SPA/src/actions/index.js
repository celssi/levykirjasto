export const tableConfigAction = (tableConfig={}) => {
    return {
        type: 'TABLE_CONFIG',
        payload: tableConfig
    }
};

export const searchAction = (searchPhrase='') => {
    return {
        type: 'SEARCH',
        payload: searchPhrase
    }
};
