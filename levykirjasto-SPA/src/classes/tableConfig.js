export default class TableConfig {
    records;
    allRecords;
    activePage: number;

    constructor(records=[], allRecords=[]) {
        this.records = records;
        this.allRecords = allRecords;
        this.activePage = 1;
    }
}
