import React from 'react';
import {Button} from 'semantic-ui-react';
import RecordModal from "./RecordModal";

class Buttons extends React.Component {
    modify = () => {
      console.log(this.props.recordId);
    };

    delete = () => {
        console.log(this.props.recordId);
    };

    render() {
        return (
            <div>
                <RecordModal />
                <Button negative onClick={this.delete}>Poista</Button>
            </div>
        );
    }
}

export default Buttons;
