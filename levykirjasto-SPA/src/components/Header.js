import React from 'react';
import './Header.css';

class Header extends React.Component {
    render() {
        return (
            <div className="ui fixed inverted menu">
                <div className="ui container">
                    <button className="header item" onClick={this.onClick}>{this.props.title}</button>
                    <button className="item" id="cd" onClick={this.onClick}>CD:t</button>
                    <button className="item" id="records" onClick={this.onClick}>Levyt</button>
                    <button className="item" id="books" onClick={this.onClick}>Kirjat</button>
                    <button className="item" id="manga" onClick={this.onClick}>Manga</button>
                    <button className="item" id="games" onClick={this.onClick}>Pelit</button>
                </div>
            </div>
        );
    };

    onClick = (e) => {
        console.log(e.target.id);
    };
}

export default Header;
