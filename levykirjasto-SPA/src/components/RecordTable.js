import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderCell,
    TableRow,
    TableCell,
    Segment,
    Dimmer,
    Loader, Image, Pagination
} from 'semantic-ui-react';
import { FaAngleDown, FaAngleUp } from 'react-icons/fa';
import {connect} from 'react-redux';

import Buttons from './Buttons';
import {searchAction, tableConfigAction} from '../actions';

class RecordTable extends React.Component {
    render() {
        const recordCount = this.props.records.length;
        const numberOfPages = Math.ceil(recordCount/50);
        const { activePage } = this.props;

        if (recordCount === 0) {
            return (
                <Segment>
                    <Dimmer active>
                        <Loader/>
                    </Dimmer>
                    <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png'/>
                </Segment>
            );
        }

        return (
            <Table celled padded>
                <TableHeader>
                    <TableRow>
                        <TableHeaderCell id="ean" onClick={this.sort}>Viivakoodi { this.getSortIcon('ean') }</TableHeaderCell>
                        <TableHeaderCell id="name" onClick={this.sort}>Nimi { this.getSortIcon('name') }</TableHeaderCell>
                        <TableHeaderCell id="type" onClick={this.sort}>Tyyppi { this.getSortIcon('type') }</TableHeaderCell>
                        <TableHeaderCell>Toiminnot</TableHeaderCell>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    { this.renderRows() }
                </TableBody>
                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan='4'>
                            <Pagination
                                floated='right'
                                totalPages={numberOfPages}
                                activePage={activePage}
                                onPageChange={this.handlePaginationChange}
                            />
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        );
    }

    slice = (records, activePage) => {
        const start = activePage*50 - 50;
        const end = start + 50;
        return records.slice(start, end);
    };

    renderRows = () => {
        const slicedRecords = this.slice(this.props.searchPhrase.length > 0 ? this.props.records : this.props.allRecords, this.props.activePage);

        return slicedRecords.map(record => {
            return (
                <TableRow key={record.id}>
                    <TableCell>{record.ean}</TableCell>
                    <TableCell>{record.name}</TableCell>
                    <TableCell>{record.type}</TableCell>
                    <TableCell><Buttons recordId={record.id}/></TableCell>
                </TableRow>
            );
        });
    };

    handlePaginationChange = (e, { activePage }) => {
        this.props.handlePaginationChange(activePage);
    };

    sort = (e) => {
      const target = e.target.id;
      this.props.onSort(target);
    };

    getSortIcon = (column) => {
        if (this.props.sortColumn === column) {
            return this.props.sortDesc ? <FaAngleUp /> : <FaAngleDown />;
        }
    }
}

const mapStateToProps = (state) => {
    return {
        records: state.tableConfig.records,
        allRecords: state.tableConfig.allRecords,
        searchPhrase: state.searchPhrase,
        activePage: state.tableConfig.activePage
    };
};

export default connect(mapStateToProps, {
    tableConfigAction,
    searchAction
})(RecordTable);
