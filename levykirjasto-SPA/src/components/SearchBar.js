import React from 'react';
import {FormField} from "semantic-ui-react";

class SearchBar extends React.Component {
    state = { term: '' };

    onInputChange = (event) => {
        this.setState( { term: event.target.value }, this.sendValueToParent);
    };

    sendValueToParent = () => {
        this.props.onChange(this.state.term);
    };

    onFormSubmit = (event) => {
        event.preventDefault();
    };

    render() {
        return (
            <div className="ui segment">
                <form className="ui form" onSubmit={this.onFormSubmit}>
                    <FormField>
                        <div className="ui icon input">
                            <input type="text" value={this.state.term} placeholder="Etsi..." onChange={this.onInputChange}/>
                            <i aria-hidden="true" className="search icon"/>
                        </div>
                    </FormField>
                </form>
            </div>
        );
    }
}

export default SearchBar;
