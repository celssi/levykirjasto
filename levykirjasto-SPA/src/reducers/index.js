import { combineReducers} from 'redux';
import TableConfig from '../classes/tableConfig';

// https://github.com/reduxjs/redux-thunk

const tableConfigReducer = (tableConfig: TableConfig = new TableConfig(), action) => {
    if (action && action.type === 'TABLE_CONFIG') {
        action.payload.records = action.payload.records ? action.payload.records : tableConfig.records;
        action.payload.allRecords = action.payload.allRecords ? action.payload.allRecords : tableConfig.allRecords;

        return action.payload;
    }

    return tableConfig;
};

const searchReducer = (searchPhrase='', action) => {
    if (action && action.type === 'SEARCH') {
        return action.payload;
    }

    return searchPhrase;
};

export default combineReducers({
   tableConfig: tableConfigReducer,
   searchPhrase: searchReducer
});
