package com.levykirjasto.controllers;

import com.levykirjasto.dao.RecordDao;
import com.levykirjasto.entities.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class RecordController {

    @Autowired
    private RecordDao recordDaoDao;

    @RequestMapping(value = "/api/records", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Record> getRecords() {
        return (ArrayList<Record>) recordDaoDao.findAllByOrderByName();
    }

    @RequestMapping(value = "/api/records/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Record getRecord(@PathVariable Long id) {
        Optional<Record> result = recordDaoDao.findById(id);
        return result.orElse(null);
    }

    @RequestMapping(value = "/api/records/searchbytype/{type}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Record> searchRecordByType(@PathVariable int type) {
        return (ArrayList<Record>) recordDaoDao.findByType(type);
    }

    @RequestMapping(value = "/api/records", method = RequestMethod.POST)
    @ResponseBody
    public Record createRecord(@RequestBody Record record) {
        try {
            Record r = new Record(record.getEan(), record.getType(), record.getName());
            r = recordDaoDao.save(r);
            return r;
        }

        catch (Exception e) {
            throw e;
        }
    }

    @RequestMapping(value = "/api/records", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Resource> updateRecord(@RequestBody Record record) {
        recordDaoDao.save(record);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/api/records/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteRecord(@PathVariable Long id) {
        Optional<Record> result = recordDaoDao.findById(id);
        result.ifPresent(record -> recordDaoDao.delete(record));
    }
}