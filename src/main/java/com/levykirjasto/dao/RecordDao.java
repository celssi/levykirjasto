package com.levykirjasto.dao;

import com.levykirjasto.entities.Record;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface RecordDao extends CrudRepository<Record, Long> {
    List<Record> findAllByOrderByName();
    Optional<Record> findById(Long id);
    List<Record> findByName(String name);
    Record findByEan(String ean);
    List<Record> findByType(int type);
}